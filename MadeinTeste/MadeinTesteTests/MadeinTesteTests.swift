//
//  MadeinTesteTests.swift
//  MadeinTesteTests
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import XCTest
import RxTest
import RxCocoa
import RxBlocking
import RxSwift
@testable import MadeinTeste

class MadeinTesteTests: XCTestCase {
    var vc:DummyViewController!
    var viewModel:SearchViewModel!
    var searchListResponse:SearchListResponse!
    
    override func setUp() {
        super.setUp()
        vc = DummyViewController()
        let data = loadStubFromBundle(withName: "youtube", extension: "json")
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.youtubeDateFormatter)
        searchListResponse = try! jsonDecoder.decode(SearchListResponse.self, from: data)
        viewModel = SearchViewModel(view: vc, service: SearchVideoService())
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testEnterInvalidSearch_ExpectFalse(){
        viewModel.searchQuery.value = ""
        _ = viewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            XCTAssertEqual(isValid, false)
        })
        
    }
    
    func testEnterValidSearch_ExpectTrue(){
        viewModel.searchQuery.value = "Teste"
        _ = viewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            XCTAssertEqual(isValid, true)
        })
    }
    
    func testEnterLessThanTreeSearch_ExpectFalse(){
        viewModel.searchQuery.value = "Te"
        _ = viewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            XCTAssertEqual(isValid, false)
        })
    }
    
    func testDataFromAPI_ExpectNotNil(){
        XCTAssertNotNil(searchListResponse)
    }
    
    func testDataFromAPI_ExpectChanelTitleNotNil(){
        let item = searchListResponse.items.first!
        XCTAssertNotNil(item.snippet.channelTitle)
  
    }
}

extension XCTestCase{
    func loadStubFromBundle(withName name:String , extension:String)->Data{
        
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: `extension`)
        return try! Data(contentsOf: url!)
    }
}
