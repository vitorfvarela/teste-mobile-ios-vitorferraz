//
//  DummyViewController.swift
//  MadeinTesteTests
//
//  Created by Vitor Ferraz on 23/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
@testable import MadeinTeste

class DummyViewController: UIViewController ,SearchPresenterProtocol{
    func goToListViewController(withList list: SearchListResponse) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
