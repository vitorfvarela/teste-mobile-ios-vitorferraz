//
//  Constants.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
struct K {
    struct ProductionServer {
        static let baseURL = "https://www.googleapis.com/youtube/v3/"
    }
    
    
    struct APIKey {
        static let  key = "AIzaSyAQuYgvWIx4TGpYRIzMP-icJc6H9TYhLHw"
        
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
