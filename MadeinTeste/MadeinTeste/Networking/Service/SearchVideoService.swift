//
//  SearchVideoService.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Alamofire

class SearchVideoService{

    let endPoint: YouTubeAPI = .search
    
    func performSearch(withQuery query: String, nextPageToken: String? = nil, completion:@escaping (Result<SearchListResponse>)->Void) {
        
        var parameters: [String : Any] = [:]
        
        parameters["pageToken"] = nextPageToken
        parameters["q"] = query
        parameters["part"] = "snippet"
        parameters["maxResults"] = 25
        parameters["type"] = "video"
        parameters["key"] = K.APIKey.key
        parameters["pageToken"] = nextPageToken
   
        
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.youtubeDateFormatter)
        
        APIClient.performRequest(endPoint: endPoint, parameters: parameters, decoder: jsonDecoder, completion: completion)
        
    }
}
