//
//  VideoDetailService.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Alamofire
class VideoService {
    
    let endPoint: YouTubeAPI = .videos
    
    func fetchVideo(withIdentifier identifier: Identifier, completion:@escaping (Result<VideoListResponse>)->Void) {
        
        var parameters: [String : Any] = [:]
        
        parameters["id"] = identifier.videoId
        parameters["part"] = ["snippet", "statistics"].joined(separator: ",")
        parameters["key"] = K.APIKey.key
        
        APIClient.performRequest(endPoint: endPoint, parameters: parameters, completion: completion)

    }
}

