//
//  UserEndpoint.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Alamofire
enum YouTubeAPI: String, URLConvertible {
    case search, videos
    
    static let baseURL = "https://www.googleapis.com/youtube/v3/"
    
    func asURL() throws -> URL {
        
        return try YouTubeAPI.baseURL.appending(self.rawValue).asURL()
    }
}
