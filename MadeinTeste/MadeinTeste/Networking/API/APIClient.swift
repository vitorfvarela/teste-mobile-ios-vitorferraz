//
//  APIClient.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Alamofire




class APIClient {
    @discardableResult
     static func performRequest<T:Decodable>(endPoint:YouTubeAPI,parameters:[String:Any]? = nil, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T>)->Void) -> DataRequest {
    
        return  Alamofire.request(endPoint, method: .get, parameters: parameters).responseJSONDecodable(decoder: decoder){ (response: DataResponse<T>) in
            //dump(response)
                completion(response.result)    
        }
    }
    

}
