//
//  UIExtensions.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import Kingfisher
extension UIView{
    func cropProcessor() -> CroppingImageProcessor{
        return CroppingImageProcessor(size: CGSize(width: 1000, height: 1000), anchor: CGPoint(x: 0.5, y: 0.5))
        
    }
}

extension UIViewController{
    
    func showSimpleAlert (title:String, message:String, viewController: UIViewController, completion:((UIAlertAction) -> Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "CANCELAR", style: .destructive, handler: nil))
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: completion))
        
        
        viewController.present(alert, animated: true, completion: nil)
    }
}
extension UIViewController{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboardController))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboardController() {
        view.endEditing(true)
    }
}

extension UIButton{
    func disabledButton(){
        self.backgroundColor = .lightGray
        self.isEnabled = false
    }
    
    func enabledButton(originalColor:UIColor?){
        self.backgroundColor = originalColor ?? .blue
        self.isEnabled = false
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
