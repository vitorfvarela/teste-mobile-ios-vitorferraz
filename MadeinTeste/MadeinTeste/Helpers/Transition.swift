//
//  Transition.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
class TransitionManager: UIStoryboardSegue {
    
    
    override func perform() {
        
        let sourceController = self.source as? SearchVideoViewController
        
        let firstVCView = self.source.view as UIView?
        let secondVCView = self.destination.view as UIView?
        
        
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        
        secondVCView?.frame = CGRect(x: 0.0, y: screenHeight, width: screenWidth, height: screenHeight)
        
        
        
        guard let secondView = secondVCView,
            let firstView = firstVCView else {
                return
        }
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(firstView, aboveSubview: secondView)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.8, animations: { () -> Void in
            sourceController?.searchButton?.alpha = 0
            sourceController?.searchMovieTfd?.alpha = 0


            
            sourceController?.imageLogo?.frame.origin.y -= 104
            sourceController?.imageLogo?.frame.size.height -= 35
            sourceController?.imageLogo?.frame.size.width -= 150
            sourceController?.imageLogo?.center.x = firstView.center.x
            

        }) { (Finished) -> Void in
            self.source.present(self.destination as UIViewController,
                                animated: false,
                                completion: nil)
        }
        //super.perform()
    }
    
}
