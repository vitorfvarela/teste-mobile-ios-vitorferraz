//
//  BaseView.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

protocol BaseView {
    var activityData:ActivityData {get}
    
    func showLoading()
    func hideLoading()
    func showDefaultError()
    func showError(withMessage message:String)

}


extension UIViewController:BaseView{
    
    var activityData: ActivityData {
        return ActivityData()
    }
    
    func showLoading(){
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
    }
    
    func hideLoading(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func showDefaultError(){
        self.showSimpleAlert(title: "Ops!", message: "Ocorreu um erro, por favor tente novamente", viewController: self, completion: nil)
    }
    func showError(withMessage message:String){
        self.showSimpleAlert(title: "Ops!", message: message, viewController: self, completion: nil)
    }
    
}
