//
//  VideoTableViewCell.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import Kingfisher

protocol VideoCellRepresentable{
    var image:URL{get}
    var title:String{get}
    var description:String{get}
}

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageCover: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(withModel model:VideoCellRepresentable){
      self.imageCover.kf.setImage(with: model.image, placeholder: UIImage(named: "notAvaliable"), options: [.transition(.fade(0.2)), .processor(cropProcessor())], progressBlock: nil, completionHandler: nil)
        self.titleLabel.text = model.title
        self.descriptionLabel.text = model.description
    }
    
    static func register(to tableView:UITableView) {
        let nib = UINib(nibName: "VideoTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "VideoTableViewCell")
    }
    
    
}
