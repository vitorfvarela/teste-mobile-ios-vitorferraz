//
//  ListVideoViewController.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class ListVideoViewController: UIViewController {
    
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchVideoTfd: UITextField!
    @IBOutlet weak var tableView: UITableView!
    private var dataSource:TableViewDataSource<VideoTableViewCell, VideoViewModel>!
    var videoListViewModel:VideoListViewModel!
    private let disposeBag = DisposeBag()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        VideoTableViewCell.register(to: tableView)
        self.updateList()
        setupBindElements()
        searchVideoTfd.delegate = self
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }


    @IBAction func returnToSearchViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
 
    func configureSearchField(){
       
        searchVideoTfd
            .rx.text // Observable property thanks to RxCocoa
            .orEmpty // Make it non-optional
            .debounce(0.75, scheduler: MainScheduler.instance)// Wait 0.2 for changes.
            .distinctUntilChanged() // If they didn't occur, check if the new value is the same as old.
            .filter {
                if !$0.isEmpty{
                    return true
                }
                
                self.videoListViewModel.fetchList()
                
                return false
            } // If the new value is really new, filter for non-empty query.
            .subscribe(onNext: { [unowned self] query in // Here we will be notified of every new value
               
                self.videoListViewModel.fetchList()

            }).disposed(by: disposeBag)
    }
    
    func setupBindElements(){
        _ = searchVideoTfd.rx.text.map{$0 ?? ""}.bind(to: videoListViewModel.searchQuery)
        _ = videoListViewModel.isValid.bind(to: searchButton.rx.isEnabled)
        searchButton.disabledButton()
        _ = videoListViewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            self.searchButton.isEnabled = isValid ? true : false
            self.searchButton.backgroundColor = isValid ? UIColor(rgb: 0x080FF) : .lightGray
        })
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @IBAction func performSearchButtonTapped(_ sender: UIButton) {
            videoListViewModel.fetchList()
    }
    
}

extension ListVideoViewController:VideoListPresenterProtocol{
    func updateList() {
        self.dataSource = TableViewDataSource(cellIdentifier: "VideoTableViewCell", items: self.videoListViewModel.list, configureCell: { (cell, viewModel) in
            cell.setup(withModel: viewModel)
            
        })
        self.tableView.dataSource = dataSource
        self.tableView.delegate = self
        self.tableView.reloadData()
        self.searchVideoTfd.text = videoListViewModel.searchQuery.value
        
    }
    
    
}

extension ListVideoViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text != videoListViewModel.currentQueryValue{
            configureSearchField()
        }
        return true
    }
}

extension ListVideoViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToVideoDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.videoListViewModel.list.count - 1 {
            videoListViewModel.fetchList(nextPage: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToVideoDetail"{
            guard let targetController = segue.destination as? VideoDetailViewController else{return}
            
            guard let index = tableView.indexPathForSelectedRow else{return}
            print(index.row)
            let item = videoListViewModel.getMovieAt(index: index.row)
            targetController.videoDetailViewModel = VideoDetailViewModel(view: targetController, service: VideoService(), id: item.id, searchResult: item)
           
        }
    }
}
