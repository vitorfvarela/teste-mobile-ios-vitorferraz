//
//  VideoListViewModel.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import RxSwift

protocol VideoListPresenterProtocol:BaseView {
    func updateList()
    
}

class VideoListViewModel:BasePresenter {
    var view: VideoListPresenterProtocol
    var service: SearchVideoService
    
    typealias V = VideoListPresenterProtocol
    typealias S = SearchVideoService
    private var listModel: SearchListResponse!
    var searchQuery = Variable<String>("")
    var currentQueryValue = ""
    var isValid:Observable<Bool>{
        return searchQuery.asObservable().map({$0.count >= 3})
    }
    
    var list:[VideoViewModel]{
        return self.listModel.items.map{VideoViewModel(video: $0)}
    }
    
    
    init(view:VideoListPresenterProtocol,service:SearchVideoService,listModel:SearchListResponse,query:Variable<String>) {
        self.view = view
        self.service = service
        self.listModel = listModel
        self.searchQuery = query
        self.currentQueryValue = query.value
        
       
    }
    
    func getMovieAt(index:Int)->SearchResult{
        return self.listModel.items[index]
    }
    
    func fetchList(nextPage:Bool = false){
        let nextPageToken:String? = nextPage ? "CAoQAA" : nil
        
        view.showLoading()
        service.performSearch(withQuery: searchQuery.value,nextPageToken: nextPageToken) { [weak self](result) in
            self?.view.hideLoading()
            switch result{
                
            case .success(let list):
                if nextPageToken != nil{
                    self?.listModel.items += list.items
                }else{
                    self?.listModel = list
                }
                self?.view.updateList()
            case .failure(let error):
                print(error)
                self?.view.showDefaultError()
                
            }
        }
    }
    
    
    
    
}


class VideoViewModel:VideoCellRepresentable{
    var image: URL
    var title: String
    var description: String
    var video:SearchResult!
    
    init(video:SearchResult) {
        self.video = video
        self.title = video.snippet.channelTitle
        self.image = (video.snippet.thumbnails.medium?.url ?? URL(string: "notAvaliable"))!
        self.description = video.snippet.description ?? "Não disponível"
        
    }
    
    
}
