//
//  Video.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
struct VideoListResponse: Codable {
    let items: [VideoResult]
}

struct VideoResult: Codable {
    let id: String
    let snippet: Snippet
    let statistics: Statistics?
}

struct Statistics: Codable {
    let viewCount: String
    let likeCount: String
    let dislikeCount:String
}
