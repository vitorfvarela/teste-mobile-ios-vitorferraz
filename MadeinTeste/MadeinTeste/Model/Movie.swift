//
//  Movie.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation


struct SearchListResponse: Codable {
    let nextPageToken: String?
    let items: [SearchResult]
}

struct SearchResult: Codable {
    let id: Identifier
    let snippet: Snippet
}

struct Identifier: Codable {
    let videoId: String
}

struct Snippet: Codable {
    let publishedAt: Date
    let channelId: String
    let title: String
    let description: String?
    let thumbnails: Thumbnails
    let channelTitle: String
}

struct Thumbnails: Codable {
    let low: ThumbnailDetail?
    let medium: ThumbnailDetail?
    let high: ThumbnailDetail?
    let standard: ThumbnailDetail?
    let maxres: ThumbnailDetail?
    
    enum CodingKeys: String, CodingKey {
        case low = "default", medium, high, standard, maxres
    }
}

struct ThumbnailDetail: Codable {
    let url: URL
    let width: Int
    let height: Int
}


