//
//  VideoDetailViewController.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import Kingfisher
import WebKit

class VideoDetailViewController: UIViewController {

    @IBOutlet weak var channelTitleLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberOfLikesLabel: UILabel!
    @IBOutlet weak var numberOfDislikesLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var videoDetailViewModel:VideoDetailViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        videoDetailViewModel.fetchVideoDetail()
        let logo = #imageLiteral(resourceName: "madeinweb")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        imageView.contentMode = .scaleAspectFit
        imageView.image = logo
        
        self.navigationItem.titleView = imageView
        activityIndicatorView.startAnimating()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension VideoDetailViewController:DetailPresenterProtocol{
    func fetchedVideo() {
        print("video carregado com sucesso")
        self.descriptionLabel.text = videoDetailViewModel.description
        self.titleLabel.text = videoDetailViewModel.title
        self.numberOfLikesLabel.text = videoDetailViewModel.likeCount
        self.numberOfDislikesLabel.text = videoDetailViewModel.dislikeCount
        self.thumbnailImageView.kf.setImage(with: videoDetailViewModel.thumbnailURL, placeholder: UIImage(named: "notAvaliable"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        self.channelTitleLabel.text = videoDetailViewModel.channelTitle
        loadVideo(videoDetailViewModel.videoId.videoId)

    }
    
    func loadVideo(_ id:String) {
        let url = URL(string: "https://www.youtube.com/embed/\(id)?playsinline=1")
        
        guard let unpackedUrl = url else {
            return
        }
        let request = URLRequest(url: unpackedUrl)
        thumbnailImageView?.removeFromSuperview()
        activityIndicatorView.stopAnimating()
        webView?.loadRequest(request)
    }
    
    
}


// MARK: - Bar positioning delegate
extension VideoDetailViewController: UIBarPositioningDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

