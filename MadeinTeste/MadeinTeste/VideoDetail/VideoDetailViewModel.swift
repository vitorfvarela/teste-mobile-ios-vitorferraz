//
//  VideoDetailViewModel.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation

protocol DetailPresenterProtocol:BaseView {
    func fetchedVideo()
    
}

class VideoDetailViewModel :BasePresenter{
    var view: DetailPresenterProtocol
    var service: VideoService
    
    typealias V = DetailPresenterProtocol
    typealias S = VideoService
    
     var videoId:Identifier
    private var video:VideoResult!
    private var searchResult:SearchResult!
    
    init(view:DetailPresenterProtocol,service:VideoService,id:Identifier,searchResult:SearchResult) {
        self.view = view
        self.service = service
        self.videoId = id
        self.searchResult = searchResult
    }
    
    var title:String{
        return video.snippet.title
    }
    var description: String {
        return video.snippet.description ?? NSLocalizedString("Not Avaliable", comment: "Not Avaliable Text")
    }
    var thumbnailURL: URL {
        return (searchResult.snippet.thumbnails.medium?.url ?? URL(string: "notAvaliable"))!
    }
    var channelTitle: String {
        return searchResult.snippet.channelTitle
    }
    
    var videoEmbedURLRequest: URLRequest? {
        let id = searchResult.id.videoId
        guard let url = URL(string: "https://www.youtube.com/embed/\(id)") else {
            return nil
        }
        return URLRequest(url: url)
    }
    var likeCount: String {
         let likeCount = video.statistics?.likeCount ?? "0"
        let likesFormat = NSLocalizedString("%@ likes", comment: "likes count")
        return String(format: likesFormat, likeCount)
    }
    
    var dislikeCount: String {
        let dislikeCount = video.statistics?.dislikeCount ?? "0"
        let dislikeFormat = NSLocalizedString("%@ dislikes", comment: "dislikes count")
        return String(format: dislikeFormat, dislikeCount)
    }
    
    var viewCount: String? {
        guard let viewCount = video.statistics?.viewCount else {
            return nil
        }
        let viewsFormat = NSLocalizedString("%@ views", comment: "views count")
        return String(format: viewsFormat, viewCount)
    }
    
 
    
    func fetchVideoDetail(){
        view.showLoading()
        service.fetchVideo(withIdentifier: videoId) { (result) in
            self.view.hideLoading()
            switch result{
            case .failure(let error):
                print(error)
                self.view.showDefaultError()
            case .success(let list):
                //dump(list)
                guard let video = list.items.first else{
                    self.view.showDefaultError()
                    return
                }
                self.video = video
                self.view.fetchedVideo()
                //dump(video)
            }
        }
    }
    
    
    
}


