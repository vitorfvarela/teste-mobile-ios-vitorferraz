//
//  SearchViewModel.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import RxSwift

protocol SearchPresenterProtocol:BaseView {
    func goToListViewController(withList list:SearchListResponse)
    
}

class SearchViewModel:BasePresenter{
    typealias V = SearchPresenterProtocol
    typealias S = SearchVideoService
    
    var view: SearchPresenterProtocol
    var service: SearchVideoService
    
    
    init(view:SearchPresenterProtocol,service:SearchVideoService) {
        self.view = view
        self.service = service
        
    }
    

    
    var searchQuery = Variable<String>("")
    var isValid:Observable<Bool>{
        return searchQuery.asObservable().map({$0.count >= 3})
    }
    
    func fetchList(){
        view.showLoading()
        service.performSearch(withQuery: searchQuery.value) { [weak self](result) in
            self?.view.hideLoading()
            switch result{
                
            case .success(let list):
                self?.view.goToListViewController(withList: list)
            case .failure(let error):
                print(error)
                self?.view.showDefaultError()
                
            }
        }
    }
    
}
