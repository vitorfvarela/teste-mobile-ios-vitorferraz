//
//  SearchVideoViewController.swift
//  MadeinTeste
//
//  Created by Vitor Ferraz on 22/06/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class SearchVideoViewController: UIViewController {
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchMovieTfd: UITextField!
    @IBOutlet weak var validationLabel: UILabel!
    var searchViewModel:SearchViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
       searchViewModel = SearchViewModel(view: self, service: SearchVideoService())
        setupScrollView()
        setupBindElements()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupLayout()
    }
    
    @IBAction func performSearchButtonTapped(_ sender: UIButton) {
        searchViewModel.fetchList()
    }
    

    
    func setupBindElements(){
        _ = searchMovieTfd.rx.text.map{$0 ?? ""}.bind(to: searchViewModel.searchQuery)
        _ = searchViewModel.isValid.bind(to: searchButton.rx.isEnabled)
        if searchViewModel.searchQuery.value == ""{
        searchButton.disabledButton()
        }
        _ = searchViewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            self.validationLabel.text = isValid ? "" : NSLocalizedString("Enter at least 3 letters to search", comment: "Validation To Search Video")
            self.validationLabel.textColor = isValid ? .green : .red
            self.searchButton.isEnabled = isValid ? true : false
            self.searchButton.backgroundColor = isValid ? UIColor(rgb: 0x080FF) : .lightGray
            
        })
    }

    func setupLayout(){
        guard let firstView = self.view else {return}

        UIView.animate(withDuration: 0.8, animations: { () -> Void in
            self.searchButton.alpha = 1
            self.searchMovieTfd.alpha = 1
            
            self.imageLogo.frame.origin.y += 104
            self.imageLogo.frame.size.height += 35
            self.imageLogo.frame.size.width += 150
            self.imageLogo.center.x = firstView.center.x

        })
    }
    
    func setupScrollView(){
        self.hideKeyboardWhenTappedAround()

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(SearchVideoViewController.adjustForKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(SearchVideoViewController.adjustForKeyboard(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func adjustForKeyboard(notification: NSNotification) {
        let userInfo = notification.userInfo!
        guard let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == NSNotification.Name.UIKeyboardWillHide {
            self.scrollView.contentInset = UIEdgeInsets.zero
        } else {
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset
    }
}

extension SearchVideoViewController:SearchPresenterProtocol{
    func goToListViewController(withList list: SearchListResponse) {
        //dump(list)
        performSegue(withIdentifier: "goToListVideo", sender: list)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToListVideo"{
            guard let destinationNavigationController = segue.destination as? UINavigationController else{return}
            guard let targetController = destinationNavigationController.topViewController as? ListVideoViewController else{return}
        
            guard let listViewModel = sender as? SearchListResponse else{return}
            targetController.videoListViewModel = VideoListViewModel(view: targetController, service: SearchVideoService(), listModel: listViewModel, query: searchViewModel.searchQuery)
        }
    }
    
    
    
    
}



